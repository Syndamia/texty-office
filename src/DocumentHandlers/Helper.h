#ifndef HELPER
#define HELPER

#include <pugixml.hpp>
using namespace pugi;
#include <vector>
#include <string>
using namespace std;

bool exists(const xml_node& node);

string extractDocument(const string& filePath);
ostream* openOutputStream(const string& filePath);
void closeOutputStream(ostream* outStream);

vector<string> lsDir(const string& dir);
void rmDir(const string& dir);

#define utf8setw(fw, s) setw(utf8fw(fw, s))

int utf8size(const char*);
int utf8size(const string&);
int utf8fw(int, const char*);
int utf8fw(int, const string&);

void prepend(string& s1, const string& s2);
bool endsWith(const string &str, const string &suffix);

#endif
