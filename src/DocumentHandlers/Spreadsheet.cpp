#include "Spreadsheet.h"
#include "Helper.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <utility>

void Spreadsheet::parseText(const string& filePath) {
	// TODO
}

void Spreadsheet::parseXLSX(const string& filePath) {
	string dir = extractDocument(filePath);

	// worksheetDocs
	string worksheetDir = string().append(dir).append("/xl/worksheets/");
	vector<string> sheets = lsDir(worksheetDir);
	sort(sheets.begin(), sheets.end(), [](const string& a, const string& b){
			return (a.size() < b.size()) || (a.size() == b.size() && a < b);
			});
	for (string& file : sheets) {
		if (!endsWith(file, "xml")) continue;

		worksheets.push_back(xml_document());
		prepend(file, worksheetDir);
		xml_parse_result result = worksheets.back().load_file(file.c_str());
		if (!result) throw logic_error("Couldn't open sheet");
	}

	// workbook
	xml_document workbook = xml_document();
	xml_parse_result result = workbook.load_file(string().append(dir).append("/xl/workbook.xml").c_str());
	if (!result) throw logic_error("Couldn't open workbook.xml");

	for (xml_node& sheet : workbook.child("workbook").child("sheets").children())
		worksheetNames.push_back(sheet.attribute("name").as_string());

	// sharedStrings
	xml_document sharedStringsDoc = xml_document();
	result = sharedStringsDoc.load_file(string().append(dir).append("/xl/sharedStrings.xml").c_str());
	if (!result) throw logic_error("Couldn't open sharedStrings");

	for (xml_node str : sharedStringsDoc.child("sst"))
		sharedStrings.push_back(str.child("t").child_value());

	// numFormats
	xml_document styles = xml_document();
	result = styles.load_file(string().append(dir).append("/xl/styles.xml").c_str());
	if (!result) throw logic_error("Couldn't open styles");

	// TODO: stylesheet.child(numFmts).childbyattr(numFmt, numFmtId, stylesheet.child(cellXfs).children()[cell.attr(s)].numFmtId])
	for (xml_node st : styles.child("cellXfs").children())
		numFormats.push_back(st.attribute("numFmtId").as_int());

	if (!this->leaveTemps) rmDir(dir);
}

Spreadsheet::Spreadsheet(const string& filePath, bool isText, bool leaveTemps) {
	this->leaveTemps = leaveTemps;
	if (isText) parseText(filePath);
	else        parseXLSX(filePath);
}

enum CellType {
	ct_b = 'b', ct_d = 'd', ct_e = 'e', ct_inlineStr = 'i', ct_n = 'n', ct_s = 's', ct_str = 't', ct_empty = '\0'
};

CellType getType(xml_node cell) {
	string attr = cell.attribute("t").as_string();
	if (attr[0] != '\0') return (attr.size() == 3) ? ct_str : (CellType)attr[0];

	if (exists(cell.child("f"))) return ct_str;
	if (exists(cell.child("si"))) return ct_inlineStr;
	if (*cell.child("v").child_value() != '\0') return ct_n;
	return ct_empty;
}


int Spreadsheet::writeDocument(const string& filePath) {
	throw logic_error("Writing to XLSX isn't implemented yet!");
}

string cellColumn(xml_node& cell) {
	string col = cell.attribute("r").value();
	col.erase(remove_if(col.begin(), col.end(), [](unsigned char c){return isdigit(c);}), col.end());
	return col;
}

int Spreadsheet::writeText(const string& filePath, int minColWidth) {
	ostream& out = *openOutputStream(filePath);

	int id = 0;
	for (xml_document& worksheet : worksheets) {
		out << endl << "\\section{" << worksheetNames[id] << "}" << endl << endl;

		xml_node sheetData = worksheet.child("worksheet").child("sheetData");

		vector<int> maxwidths;
		for (xml_node& row : sheetData.children()) {
			int colI = 0;
			for (xml_node cell : row.children()) {
				if (maxwidths.size() == colI) maxwidths.push_back(minColWidth);
				int sized = utf8size((getType(cell) == ct_s) ? sharedStrings[stoi(cell.child("v").child_value())] : cell.child("v").child_value());
				maxwidths[colI] = max<int>(maxwidths[colI], sized);
				colI++;
			}
		}

		for (xml_node& row : sheetData.children()) {
			out << "|";
			int colI = 0;
			for (xml_node& cell : row.children()) {
				int maxwidth = maxwidths[colI++];
				switch(getType(cell)) {
					case ct_s: {
						const string& str = sharedStrings[stoi(cell.child("v").child_value())];
						out << utf8setw(maxwidth, str) << left << str;
						break;
					}
					case ct_empty: {
						out << string(maxwidth, ' ');
						// TODO: parse <mergeCells>
						break;
					}
					case ct_n: {
						// TODO: format number
					}
					default: {
						const char* value = cell.child("v").child_value();
						out << utf8setw(maxwidth, value) << right << value;
					}
				}
				out << "|";
			}
			out << endl;
		}
		id++;
	}

	closeOutputStream(&out);
	return 0;
}
