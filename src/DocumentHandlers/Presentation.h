#ifndef PRESENTATION
#define PRESENTATION

#include <vector>
#include <pugixml.hpp>
using namespace pugi;
using namespace std;

class Presentation {
	vector<xml_document> slides;
	vector<xml_document> rels;

	bool leaveTemps;

	void parseText(const string& filePath);
	void parsePPTX(const string& filePath);

public:
	Presentation(const string& filePath, bool isText = false, bool leaveTemps = false);

	int writeDocument(const string& filePath);
	int writeText(const string& filePath, int slide = 0);
};

#endif
