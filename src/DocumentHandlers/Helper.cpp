#include "Helper.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <dirent.h>
#include <elzip.hpp>
using namespace elz;

const xml_node defnode = xml_node();
bool exists(const xml_node& node) {
	return node != defnode;
}

string extractDocument(const string& filePath) {
	string path = string("/tmp").append(filePath.substr(filePath.find_last_of("/")));
	extractZip(filePath, path);
	return path;
}

ostream* openOutputStream(const string& filePath) {
	if (filePath == "/dev/stdout")
		return &cout;
	return new ofstream(filePath);
}

void closeOutputStream(ostream* outStream) {
	if (outStream != &cout) {
		static_cast<ofstream*>(outStream)->close();
		delete outStream;
	}
}

int utf8size(const char* str) {
	// Thanks Mark Ransom: https://stackoverflow.com/a/3586973/12036073
	int count = 0;
	while (*str != 0)
	{
		if ((*str & 0xc0) != 0x80)
			++count;
		++str;
	}
	return count;
}

int utf8size(const string& str) {
	return utf8size(str.c_str());
}

int utf8fw(int fieldwidth, const char* s) {
	return fieldwidth + strlen(s) - utf8size(s);
}

int utf8fw(int fieldwidth, const string& s) {
	return fieldwidth + s.size() - utf8size(s);
}

void prepend(string& s1, const string& s2) {
	s1 = string(s2).append(s1);
}

vector<string> lsDir(const string& dir) {
	vector<string> out;

	// Thanks https://stackoverflow.com/a/612176/12036073
	DIR *dirP;
	struct dirent *ent;
	if ((dirP = opendir(dir.c_str())) != NULL) {
		while ((ent = readdir(dirP)) != NULL)
			out.push_back(ent->d_name);
		closedir(dirP);
	}

	return out;
}

// Thanks https://www.linuxquestions.org/questions/programming-9/deleting-a-directory-using-c-in-linux-248696/#post1263044
void rmDir(const string& dirPath) {
	DIR *dir;
	struct dirent *entry;
	char path[PATH_MAX];
	dir = opendir(dirPath.c_str());
	if (dir == NULL) return;

	while ((entry = readdir(dir)) != NULL) {
		if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
			snprintf(path, (size_t) PATH_MAX, "%s/%s", dirPath.c_str(), entry->d_name);
			if (entry->d_type == DT_DIR) {
				rmDir(path);
			}
			remove(path);
		}
	}
	closedir(dir);
	remove(path);
}

bool endsWith(const string &str, const string &suffix) {
	if (str.length() < suffix.length())
		return false;
	return str.compare(str.length() - suffix.length(), suffix.length(), suffix) == 0;
}
