#ifndef SPREADSHEET
#define SPREADSHEET

#include <vector>
#include <pugixml.hpp>
using namespace pugi;
using namespace std;

class Spreadsheet {
	vector<xml_document> worksheets;
	vector<string> worksheetNames;
	vector<string> sharedStrings;
	vector<int> numFormats;

	bool leaveTemps;

	void parseText(const string& filePath);
	void parseXLSX(const string& filePath);

public:
	Spreadsheet(const string& filePath, bool isText = false, bool leaveTemps = false);

	int writeDocument(const string& filePath);
	int writeText(const string& filePath, int minColWidth);
};

#endif
