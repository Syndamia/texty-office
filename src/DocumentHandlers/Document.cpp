#include "Document.h"
#include "Helper.h"
#include <iostream>
#include <cstring>

void Document::parseText(const string& filePath) {
	// TODO
}

void Document::parseDOCX(const string& filePath) {
	string dir = extractDocument(filePath);

	// document
	this->document = xml_document();
	xml_parse_result result = this->document.load_file(string().append(dir).append("/word/document.xml").c_str());
	if (!result) throw logic_error("Couldn't open file!");

	// documentRels
	this->documentRels = xml_document();
	result = this->documentRels.load_file(string().append(dir).append("/word/_rels/document.xml.rels").c_str());
	if (!result) throw logic_error("Couldn't open document.xml.rels");

	if (!this->leaveTemps) rmDir(dir);
}

Document::Document(const string& filePath, bool isText, bool leaveTemps) {
	this->leaveTemps = leaveTemps;
	if (isText) parseText(filePath);
	else        parseDOCX(filePath);
}

int Document::writeDocument(const string& filePath) {
	throw logic_error("Writing to DOCX isn't implemented yet!");
}

int Document::writeText(const string& filePath) {
	ostream& out = *openOutputStream(filePath);

	for (xml_node& pa : document.child("w:document").child("w:body").children()) {
		string style = pa.child("w:pPr").child("w:pStyle").attribute("w:val").as_string();

		string endPar = "";
		string identation = "";
		if (style == string("Heading1")) {
			out << "\\section{";
			endPar = "}";
		}
		if (exists(pa.child("w:pPr").child("w:numPr"))) {
			int numId = pa.child("w:pPr").child("w:numPr").child("w:numId").attribute("w:val").as_int();
			if (numId > 0)
				out << "  \\item ";
		}

		for (xml_node& run : pa.children()) {
			if (strcmp(run.name(), "w:hyperlink") == 0) {
				string URL = documentRels.child("Relationships").find_child_by_attribute("Relationship", "Id", run.attribute("r:id").as_string()).attribute("Target").as_string();
				string title = run.child("w:r").child("w:t").child_value();
				out << "\\href{" << URL << "}{" << title << "}";
				continue;
			}

			if (*run.child("w:t").child_value() == '\0') continue;

			string start = "";
			string str = "";
			string end = "";
			for (xml_node& elem : run.children()) {
				if (strcmp(elem.name(), "w:cr") == 0) {
					start.append("\n").append(identation);
				}
				else if (strcmp(elem.name(), "w:t") == 0) {
					str.append(elem.child_value());
				}
				else if (strcmp(elem.name(), "w:rPr") == 0) {
					if (exists(elem.child("w:strike"))) {
						prepend(start, "\\sout{");
						end.append("}");
					}
					if (exists(elem.child("w:b"))) {
						prepend(start, "\\textbf{");
						end.append("}");
					}
					if (exists(elem.child("w:i"))) {
						prepend(start, "\\textit{");
						end.append("}");
					}
					if (exists(elem.child("w:u"))) {
						prepend(start, "\\textul{");
						end.append("}");
					}
				}
			}
			if (str.size() > 0)
				out << identation << start << str << end;
		}
		out << endPar << endl;
	}

	closeOutputStream(&out);
	return 0;
}
