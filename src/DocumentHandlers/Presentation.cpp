#include "Presentation.h"
#include "Helper.h"
#include <iostream>
#include <cstring>
#include <algorithm>

void Presentation::parseText(const string& filePath) {
	// TODO
}

void Presentation::parsePPTX(const string& filePath) {
	string dir = extractDocument(filePath);

	// slides
	string slideDir = string().append(dir).append("/ppt/slides/");
	vector<string> slideFiles = lsDir(slideDir);
	sort(slideFiles.begin(), slideFiles.end(), [](const string& a, const string& b){
			return (a.size() < b.size()) || (a.size() == b.size() && a < b);
			});
	for (string& file : slideFiles) {
		if (!endsWith(file, "xml")) continue;

		slides.push_back(xml_document());
		prepend(file, slideDir);
		xml_parse_result result = slides.back().load_file(file.c_str());
		if (!result) throw logic_error(file.c_str());
	}

	// rels
	vector<string> slideRels = lsDir(slideDir.append("_rels/"));
	sort(slideRels.begin(), slideRels.end(), [](const string& a, const string& b){
			return (a.size() < b.size()) || (a.size() == b.size() && a < b);
			});
	for (string& rel : slideRels) {
		if (!endsWith(rel, "xml.rels")) continue;

		rels.push_back(xml_document());
		prepend(rel, slideDir);
		xml_parse_result result = rels.back().load_file(rel.c_str());
		if (!result) throw logic_error(rel.c_str());
	}

	if (!this->leaveTemps) rmDir(dir);
}

Presentation::Presentation(const string& filePath, bool isText, bool leaveTemps) {
	this->leaveTemps = leaveTemps;
	if (isText) parseText(filePath);
	else        parsePPTX(filePath);
}

int Presentation::writeDocument(const string& filePath) {
	throw logic_error("Writing to PPTX isn't implemented yet!");
}

void writeSlide(xml_document& slide, xml_document& rels, ostream& out) {
	for (xml_node& shape : slide.child("p:sld").child("p:cSld").child("p:spTree").children()) {
		// Paragraph
		if (strcmp(shape.name(), "p:sp") == 0) {
			unsigned autonum = 1;
			for (xml_node& pars : shape.child("p:txBody").children("a:p")) {
				if (exists(pars.child("a:pPr")) && exists(pars.child("a:pPr").child("a:buChar")))
					out << pars.child("a:pPr").child("a:buChar").attribute("char").as_string() << "  ";
				if (exists(pars.child("a:pPr")) && exists(pars.child("a:pPr").child("a:buAutoNum")))
					out << autonum++ << "  ";

				bool hasText = false;
				for (xml_node& run : pars.children("a:r")) {
					if (exists(run.child("a:t"))) {
						int close = 0;
						if (exists(run.child("a:rPr"))) {
							if (!run.child("a:rPr").attribute("b").empty() &&
								run.child("a:rPr").attribute("b").as_int() == 1) {
								out << "\\textbf{";
								close++;
							}
							if (!run.child("a:rPr").attribute("i").empty() &&
								run.child("a:rPr").attribute("i").as_int() == 1) {
								out << "\\textit{";
								close++;
							}
							if (!run.child("a:rPr").attribute("u").empty() &&
								strcmp(run.child("a:rPr").attribute("u").as_string(), "none") != 0) {
								out << "\\textul{";
								close++;
							}
							if (!run.child("a:rPr").attribute("strike").empty() &&
								strcmp(run.child("a:rPr").attribute("strike").as_string(), "noStrike") != 0) {
								out << "\\sout{";
								close++;
							}
							if (exists(run.child("a:rPr").child("a:hlinkClick"))) {
								out << "\\href{" << rels.child("Relationships")
								                        .find_child_by_attribute("Id", run.child("a:rPr")
								                                                          .child("a:hlinkClick")
								                                                          .attribute("r:id").as_string())
								                        .attribute("Target").as_string() << "}{";
								close++;
							}
						}

						out << run.child("a:t").child_value();
						while (close-- > 0) out << "}";
						hasText = true;
					}
				}
				if (hasText) out << endl;
			}
			out << endl;
		}
		// TODO: implement others
	}
}

int Presentation::writeText(const string& filePath, int slideInd) {
	ostream& out = *openOutputStream(filePath);

	if (slideInd <= 0) {
		for (unsigned i = 0; i < slides.size(); i++) {
			writeSlide(slides[i], rels[i], out);
			out << "---" << endl << endl;
		}
	}
	else {
		writeSlide(slides[slideInd-1], rels[slideInd-1], out);
	}

	closeOutputStream(&out);
	return 0;
}
