#ifndef DOCUMENT
#define DOCUMENT

#include <pugixml.hpp>
using namespace pugi;
using namespace std;

class Document {
	xml_document document;
	xml_document documentRels;

	bool leaveTemps;

	void parseText(const string& filePath);
	void parseDOCX(const string& filePath);

public:
	Document(const string& filePath, bool isText = false, bool leaveTemps = false);

	int writeDocument(const string& filePath);
	int writeText(const string& filePath);
};

#endif
