#include <getopt.h>
#include <iostream>
#include <string>
#include <cstring>
using namespace std;
#include "DocumentHandlers/Helper.h"
#include "DocumentHandlers/Document.h"
#include "DocumentHandlers/Spreadsheet.h"
#include "DocumentHandlers/Presentation.h"

enum FileType {
	NONE, DOCX, XLSX, PPTX,
};

// General options
FileType defFT = NONE;
bool leaveTemps = false;
// Document options
// Spreadsheet options
int defColWidth = -1;
// Presentation options
int slide = 0;

FileType typeFromPath(const string& filePath) {
	if (endsWith(filePath, "docx")) return DOCX;
	if (endsWith(filePath, "xlsx")) return XLSX;
	if (endsWith(filePath, "pptx")) return PPTX;
	return NONE;
}

void handleDOCX(char opt, const string& inFile, const string& outFile) {
	Document doc(inFile, opt == 't', leaveTemps);
	if (opt == 't') doc.writeDocument(outFile);
	else            doc.writeText(outFile);
}

void handleXLSX(char opt, const string& inFile, const string& outFile) {
	Spreadsheet sprsh(inFile, opt == 't', leaveTemps);
	if (opt == 't') sprsh.writeDocument(outFile);
	else            sprsh.writeText(outFile, defColWidth);
}

void handlePPTX(char opt, const string& inFile, const string& outFile) {
	Presentation prs(inFile, opt == 't', leaveTemps);
	if (opt == 't') prs.writeDocument(outFile);
	else            prs.writeText(outFile, slide);
}

int main(int argc, char* argv[]) {
	int opt;
	int lopt = 0;
	static struct option long_options[] = 
	{
		{"type"         , required_argument, NULL, 'y' },
		{"help"         , no_argument      , NULL, 'h' },
		{"office-doc"   , no_argument      , NULL, 'o' },
		{"text-doc"     , no_argument      , NULL, 't' },
		{"min-col-width", required_argument, NULL, 0 }  ,
		{"leave-temps"  , no_argument      , NULL, 0 }  ,
		{"slide"        , required_argument, NULL, 0 }  ,
		{NULL           , 0                , NULL, 0 }
	};

	while ((opt = getopt_long(argc, argv, ":hy:ot", long_options, &lopt)) != -1) 
	{
		switch (opt) 
		{
			case 0: {
				if (strcasecmp(long_options[lopt].name, "min-col-width") == 0) {
					defColWidth = atoi(optarg);
				}
				else if (strcasecmp(long_options[lopt].name, "leave-temps") == 0) {
					leaveTemps = true;
				}
				else if (strcasecmp(long_options[lopt].name, "slide") == 0) {
					slide = atoi(optarg);
				}
				break;
			}
			case 'y': {
				if      (strcasecmp(optarg, "DOCX") == 0) defFT = DOCX;
				else if (strcasecmp(optarg, "XLSX") == 0) defFT = XLSX;
				else if (strcasecmp(optarg, "PPTX") == 0) defFT = PPTX;
				else throw runtime_error("Invalid value for type option!");
				break;
			}
			case 'o': case 't': {
				string inFile = argv[optind++], outFile = (optind == argc) ? "/dev/stdout" : argv[optind];
				switch ((defFT != NONE) ? defFT : typeFromPath(inFile)) {
					case DOCX: { handleDOCX(opt, inFile, outFile); break; }
					case XLSX: { handleXLSX(opt, inFile, outFile); break; }
					case PPTX: { handlePPTX(opt, inFile, outFile); break; }
					default:
					case NONE: throw runtime_error("Invalid file type!");
				}
				break;
			}
			case '?':
			case ':': cout << "Invalid option " << (char)optopt << "!" << endl;
			case 'h': {
				/*       | 80 columns                                                                  | */
				cout << "Usage: textyoffice [OPTION] ACTION INFILE [OUTFILE]" << endl
				     << "       textyoffice (-h|--help)" << endl
				     << endl
				     << " Action:" << endl
				     << "  -o, --office-doc    Parse the input file as an Office file and output it's" << endl
				     << "                      text representation to OUTFILE" << endl
				     << "  -t, --text-doc      Parse the input file as a text representation and output" << endl
				     << "                      it's corresponding Office document to OUTFILE" << endl
				     << endl
				     << " Options:" << endl
				     << "  -y, --type VALUE    Parse INFILE as a particular type, no matter the file" << endl
				     << "                      name extension. VALUE can be one of: DOCX, XLSX, PPTX" << endl
				     << "                      (case insensitive)." << endl
				     << "  --leave-temps       Do not remove temporary files (in /tmp) that are" << endl
				     << "                      generated from the program. Currently those are the" << endl
				     << "                      extracted files from INFILE (lokated at /tmp/INFILE)." << endl
				     << " DOCX options:" << endl
				     << " XLSX options:" << endl
				     << "  --min-col-width VALUE    Minimum width for every column" << endl
				     << " PPTX options:" << endl
				     << "  --slide INDEX    Output only the INDEX'th slide, where the first slide is on" << endl
				     << "                   index 1" << endl;
				return 0;
			}
		}
	}
	return 0;
}
