# texty-office

This is a small, handmade CLI tool, which aims to convert OOXML documents (.docx, .xlsx, .pptx) to a nice to read text representation and back.
Primarily made for the [texty-office.vim](https://gitlab.com/Syndamia/texty-office.vim) plugin.

*Note*: Currently only converting to text representation is implemented, and poorly.
It's very hard-coded and underpowered, primarily due to the difficulties and development time needed to parse OOXML.
If you want to make changes, some useful resources are [officeopenxml.com](http://officeopenxml.com/), the [generated pugixml documentation](https://docs.ros.org/en/jade/api/pugixml/html/annotated.html) and [Eric White's blog](https://www.ericwhite.com/blog/introduction-to-wordprocessingml-series/).

## Install

Currently, only manual build and install is supported.
Requirements:

- gcc
- cmake
- make

Clone and compile one-liner:

```bash
git clone --recurse-submodules https://gitlab.com/Syndamia/texty-office.git && cd texty-office && cmake . && make
```

Now you should be in the `texty-office` directory, just run `./texty-office` to get going.
`./texty-office -h` for information on using the tool.
